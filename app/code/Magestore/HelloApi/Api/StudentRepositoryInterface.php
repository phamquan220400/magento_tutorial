<?php
namespace Magestore\HelloApi\Api;

use Laminas\Mail\Header\Subject;
use Magestore\HelloApi\Api\Data\StudentInterface;
use Magestore\HelloApi\Model\StudentFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magestore\HelloApi\Api\Data\StudentSearchResultInterface;

interface StudentRepositoryInterface{
    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return StudentSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);


    /**
     * @param int $studentID
     * @return StudentInterface
     */
    public function getByID(int $studentID) : StudentInterface;

    /**
     * @param int $studentID
     * @return bool true on success
     */
    public function deleteByID(int $studentID):bool;

    /**
     * @param StudentInterface $student
     * @return bool true on success
     */
    public function delete(StudentInterface $student):bool;

    /**
     * @param StudentInterface $student
     * @return StudentInterface
     */
    public function save(StudentInterface $student) : StudentInterface;
}
