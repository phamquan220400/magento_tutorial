<?php

namespace Magestore\HelloPage\Controller\Process;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey;
use Magento\Checkout\Model\Cart;
use Magento\Catalog\Model\Product;

class AddProduct extends Action
{
    protected $formKey;
    protected $cart;
    protected $product;
    protected $url;
    protected $resultRedirect;

    public function __construct(
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        Context $context,
        FormKey $formKey,
        Cart $cart,
        Product $product) {
            $this->resultRedirectFactory = $resultRedirectFactory;
            $this->formKey = $formKey;
            $this->cart = $cart;
            $this->product = $product;
            parent::__construct($context);
    }

    public function execute()
    {
        $productId = 100000;
        $params = array(
            'form_key' => $this->formKey->getFormKey(),
            'product' => $productId,
            'qty' => 1
        );
        $product = $this->product->load($productId);
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!empty($product) && $product->getId()) {
            $this->cart->addProduct($product, $params);
            $this->cart->save();
            $this->messageManager->addSuccessMessage("thanh cong");
        } else {
            $this->messageManager->addErrorMessage("khong tim thay id product");
        }
        $resultRedirect->setPath('checkout/cart');
        return $resultRedirect;
    }
}




