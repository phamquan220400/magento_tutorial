<?php

namespace Magestore\AdminCustomize\Controller\Adminhtml\Index;

use Magestore\AdminCustomize\Model\StudentFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
class Delete extends Action
{
    protected $resultPageFactory;
    protected $studentFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        StudentFactory $studentFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->studentFactory = $studentFactory;
        parent::__construct($context);

    }

    public function execute()
    {
        $resultRedirectFactory = $this->resultRedirectFactory->create();
        try {

           $id = $this->getRequest()->getParams('sutdent_id');
            if ($id['student_id']) {
                $model = $this->studentFactory->create()->load($id['student_id']);
                if ($model->getData()) {
                   $model->delete();
                    $this->messageManager->addSuccessMessage(__("Record Delete Successfully."));
                } else {
                    $this->messageManager->addErrorMessage(__("Something went wrong, Please try again."));
                }
            } else {
                $this->messageManager->addErrorMessage(__("Something went wrong, Please try again."));
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __("We can't delete record, Please try again."));
        }
        return $resultRedirectFactory->setPath('*/*/index');
    }
}
