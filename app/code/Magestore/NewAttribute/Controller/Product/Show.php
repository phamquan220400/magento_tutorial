<?php

namespace Magestore\NewAttribute\Controller\Product;
use Magento\Framework\App\Action\Context;

class Show extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected  $_attributeFactory;

    function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magestore\NewAttribute\Model\AttributeFactory $attributeFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->_attributeFactory = $attributeFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $attribute = $this->_attributeFactory->create();
        $collection = $attribute->getCollection();

        $collection->getSelect()
            ->join(array('catalog_int' => 'catalog_product_entity_int'),
                'main_table.entity_id = catalog_int.entity_id',
                array()
            )
            ->join(array('eav' => 'eav_attribute'),
                'eav.attribute_id = catalog_int.attribute_id',
                array()
            )->where('`eav`.`attribute_code` = "is_feature" and `catalog_int`.`value` = 1');
        foreach ($collection as $item) {
            echo "<pre>";
            print_r($item->getData());
            echo "</pre>";
        }
        exit();
        return $this->_pageFactory->create();
    }
}

