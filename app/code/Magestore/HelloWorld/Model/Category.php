<?php

namespace Magestore\HelloWorld\Model;

use Magento\Framework\Model\AbstractModel;
use Magestore\HelloWorld\Model\ResourceModel\Category as ResourceModel;

class Category extends AbstractModel
{
    protected function _construct()
    {
        $this->_init('Magestore\HelloWorld\Model\ResourceModel\Category');
    }
}

