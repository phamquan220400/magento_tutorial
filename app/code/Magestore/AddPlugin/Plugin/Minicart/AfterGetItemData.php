<?php

namespace Magestore\AddPlugin\Plugin\Minicart;

use Magento\Checkout\CustomerData\AbstractItem;

class AfterGetItemData
{
    public function afterGetItemData(AbstractItem $item, $result)
    {
        try {
            if ($result['product_id'] > 0) {
                $image = "https://magestore-service.atlassian.net/s/azc3hx/b/8/3e5bf54407d016d63f9c1cd2bc89e653/_/jira-logo-scaled.png";
                $result['product_image']['src'] = $image;
            }
        } catch (\Exception $e) {
        }
        return $result;
    }
}
